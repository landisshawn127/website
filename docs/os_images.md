---
id: images
title: Operating System Images
---

Merge allows you to choose from a number of standard operating system images.
The example below shows a 5 node experiment where each node has a different OS
image.

![](/img/topo/rainbow.png)

Images are specified as constraints on a node when the node is defined.

```python
from mergexp.machine import image

net.device(name, image == "debian:10")
```

The standard format for images is `os-family:version`. The versions used are
numeric, and not the code name of the distribution. For example `debian:10`
instead of `debian:buster` and `ubuntu:2004` instead of `ubuntu:focal`.

This is so you can use inequality constraints against images. For example

```python
from mergexp.machine import image

net.device(name, image >= "ubuntu:1804")
```

### Available Images

The images that are currently available on all Merge testbeds are the following

|Image Id| Distro Codename |
|:---|:---| 
|`debian:10`   | Debian Buster |
|`debian:11`   | Debian Bullseye |
|`ubuntu:1804` | Ubuntu Bionic |
|`ubuntu:2004` | Ubuntu Focal |


### Complete Example

```python
import mergexp as mx
from mergexp.machine import image
from mergexp.net import addressing, ipv4

def ubuntu(name, version):
    dev = net.device(name, image == "ubuntu:"+version)
    dev.props['group'] = version
    return dev

def debian(name, version):
    dev = net.device(name, image == "debian:"+version)
    dev.props['group'] = version
    return dev

net = mx.Topology('rainbow', addressing == ipv4)

nodes = [
    ubuntu('a','1804'),
    ubuntu('b','2004'),
    debian('c', '10'),
    debian('d', '11')
]
lan = net.connect(nodes)

mx.experiment(net)
```
