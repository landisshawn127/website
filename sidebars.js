module.exports = {
  someSidebar: {
    Concepts: [
        'overview', 
        'experimentation',
        'resources',
    ],
    'Getting Started': [
        'web',
        'cli',
        'xdc-access',
    ],
    'Topology Modeling': [
        'hello-world',
        'mpl',
        'routes',
        'emulation',
        'images',
        'fattree',
    ],
    'Experiment Development': [
        'automation-basics',
        'experiment-tools',
    ],
    'CLI Reference': [
        'cli-ref', 
        'xdc-ref',
    ],
    'Portal Operations': [
        'portal',
        'package-server',
        'cert-management',
    ],
    'Facility Operations': [
        'facility',
        'package-server',
        'cert-management',
    ],
    'Merge Development': [
        'dev-overview',
        'core-values',
        'vte',
        'api'
    ],
  },
};
